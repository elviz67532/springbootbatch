package com.example.demo.controller;

import com.example.demo.dao.PeopleDao;
import com.example.demo.entity.People;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class JobLauncherController {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job personJob;

    @Autowired
    private PeopleDao peopleDao;

    @GetMapping("/launchjob")
    public void launchJob() throws Exception {
        JobParameters jobParameters = new JobParametersBuilder()
                .addDate("date", new Date())
                .toJobParameters();
        jobLauncher.run(personJob, jobParameters);
    }

    @GetMapping("/people")
    public List<People> people() {
        return peopleDao.findAll();
    }
}
