package com.example.demo.Processor;

import com.example.demo.bean.Person;
import com.example.demo.entity.People;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

@Slf4j
public class PersonItemProcessor implements ItemProcessor<Person, People> {

    @Override
    public People process(final Person person) {
        final String firstName = person.firstName().toUpperCase();
        final String lastName = person.lastName().toUpperCase();
        final People people = new People();
        people.setFirstName(firstName);
        people.setLastName(lastName);

        log.info("Converting (" + person + ") into (" + people + ")");

        return people;
    }

}