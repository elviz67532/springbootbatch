package com.example.demo.dao;


import com.example.demo.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleDao extends JpaRepository<People, Long> {
}
