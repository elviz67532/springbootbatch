# SpringBoot Batch
---
## Overview
This project is a simple example of a Spring Boot Batch application. It reads data from a CSV file, processes it and writes it to a database.
## Prerequisites
- Java 17
- Spring Boot 3.2.3
## Technology Summaries
- SpringBoot Batch is compose by 3 main components:
  - Job: A job is a sequence of steps. A job can be executed multiple times with different parameters.
  - Step: A step is an independent, self-contained task that can be executed as part of a job. A step typically reads data, processes it, and writes it to a database.
  - ItemReader: An ItemReader reads data from a data source. In this example, I use a FlatFileItemReader to read data from a CSV file.
  - ItemProcessor: An ItemProcessor processes data. In this example, I use a ItemProcessor to process the data read from the CSV file.
  - ItemWriter: An ItemWriter writes data to a data source. In this example, I use a JpaItemWriter to write data to a database.
- Hint
  - Spring boot run the batch job automatically when the application starts by default, so I set spring.batch.job.enabled=false in application.yml.
  - Job will not execute twice if the job name and job parameter are same, so JobParameters set date parameter to make it unique.
## How to use
- run this project
- open the browser and access http://localhost:8080/launchjob to import data from the CSV file to the database.
- open the browser and access http://localhost:8080/persons to see the data imported from the CSV file.